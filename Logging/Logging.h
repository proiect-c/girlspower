#pragma once
#include <string>
#include <iostream>
#include <iomanip>
#include<mutex>
#include <ctime>
#pragma warning(disable : 4996)
#ifdef LOGGING_EXPORTS
#define LOGGING_API _declspec(dllexport)
#else
#define LOGGING_API _declspec(dllimport)
#endif


class LOGGING_API Logging
{
public:
    enum class Level
    {
        Info,
        Warning,
        Error
    };

public:
	template<class ... Args>
	static void Log(Level level, Args&& ... params)
	{
		switch (level)
		{
		case Level::Info:
			std::cout << "[Info]";
			break;
		}
		std::time_t crtTime = std::time(nullptr);
		std::cout << '[' << std::put_time(std::localtime(&crtTime), "%Y-%m-%d %H:%M:%S") << ']';
		std::cout << '\n';
	}

public:
    Logging(std::ostream& os, Level minimumLevel = Level::Info);

    void log(const std::string& message, Level level);

    void setMinimumLogLevel(Level minimumLevel);

private:
    std::ostream& os;
    Level minimumLevel;
	//static std::mutex m_mutex;

};

template<class ... Args>
void logi(Args&& ... params)
{
	Logging::Log(Logging::Level::Info, std::forward<Args>(params)...);
}