#include "Logging.h"

Logging::Logging(std::ostream& os, Level minimumLevel)
    : os(os), minimumLevel(minimumLevel)
{
}

void Logging::log(const std::string& message, Level level)
{
    if (level >= minimumLevel)
        os << message << std::endl;
}

void Logging::setMinimumLogLevel(Level minimumLevel)
{
    this->minimumLevel = minimumLevel;
}
