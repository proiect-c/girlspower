#include "pch.h"
#include "CppUnitTest.h"
#include "../Logging/Logging.h"
#include "../Logging/Logging.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;

namespace UnitTest
{
	TEST_CLASS(UnitTest)
	{
	public:
		
		TEST_METHOD(TestMethod1)
		{
			std::stringstream test;
			Logging logger(test,Logging::Level::Info);
			logger.log("This is a test!", Logging::Level::Info);
			std::string result;
			std::getline(test, result);
			std::string string("This is a test!");
			Assert::AreEqual(result, string);
			
		}
	};
}
